let collection = [];

// Write the queue functions below.
// Note: Avoid using Array methods (except .length) on creating the queue functions.

function print(){
	return collection;
}

function enqueue(value){

	collection[collection.length] = value;
		return collection;
}

function dequeue(){
	let updatedCollection = [];
	for (let i = 0; i < collection.length; i++){
		if (i > 0){
			updatedCollection[i-1] = collection[i];
		}
	}
	collection = updatedCollection;
	return collection;
}

function front(){
	return collection[0];
}
 
function size(){
	return collection.length;
}

function isEmpty(){
	return collection.length === 0;
}

module.exports = {
	//export created queue functions
	print,
	enqueue,
	dequeue,
	size,
	isEmpty
};